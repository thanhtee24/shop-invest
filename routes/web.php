<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('products.index');
});

//Route::resource('products', '\App\Http\Controllers\ProductsController');
Route::name('products.')->group(function () {
    Route::get('/products/{id}/show', '\App\Http\Controllers\ProductsController@show')->name('show');
    Route::get('/products/create', '\App\Http\Controllers\ProductsController@create')->name('create');
    Route::get('/products/index', '\App\Http\Controllers\ProductsController@index')->name('index');
    Route::post('/products/store', '\App\Http\Controllers\ProductsController@store')->name('store');
    Route::get('/products/{id}/edit', '\App\Http\Controllers\ProductsController@edit')->name('edit');
    Route::post('/products/{id}/update', '\App\Http\Controllers\ProductsController@update')->name('update');
    Route::get('/products/{id}/destroy', '\App\Http\Controllers\ProductsController@destroy')->name('destroy');
    Route::get('/products/{id}/view', '\App\Http\Controllers\ProductsController@view')->name('view');
    Route::get('/products/{id}/add-image', '\App\Http\Controllers\ProductsController@addImage')->name('add-image');
    Route::get('/products/{id}/update-image', '\App\Http\Controllers\ProductsController@updateImage')->name('update-image');
});

Route::name('images.')->group(function () {
    Route::get('/images/create-image', '\App\Http\Controllers\ImagesController@createImage')->name('create-image');
    Route::post('/images/upload-image', '\App\Http\Controllers\ImagesController@uploadImage')->name('upload-image');
});

Route::get('/products/{productId}/add-to-cart', '\App\Http\Controllers\ProductsController@getAddtoCart')->name('add-to-cart');


