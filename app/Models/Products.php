<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use SoftDeletes;

    protected $table = "products";

    protected $fillable = [
        'name', 'price', 'description', 'brand_id'
    ];

    public function brand(){
        return $this->hasOne(Brands::class, 'id', 'brand_id');
    }
}
