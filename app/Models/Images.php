<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Images extends Model
{
    use SoftDeletes;

    const FOLDER_NAME = 'public';

    protected $table = 'images';

    protected $fillable = ['name', 'location'];

    public function scopeImagesSelection($query)
    {
        return $query->pluck('name', 'id');
    }
}
