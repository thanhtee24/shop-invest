<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Models\Brands;
use App\Models\Images;
use App\Models\Products;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Products::select('id', 'name', 'brand_id', 'price', 'description')->get();
        return view('products.index', ['products' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create', ['brands' => Brands::pluck('name', 'id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();

        Products::create($input);

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::find($id);
        if (empty($product)) {
            return redirect(route('404'));
        }

        return view('products.detail')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Products::find($id);
        if (empty($product)) {
            return redirect(route('404'));
        }

        return view('products.edit')->with(['product' => $product, 'brands' => Brands::pluck('name', 'id')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $product = Products::find($id);
        if (empty($product)) {
            return redirect(route('404'));
        }
        $product->update($input);


        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::find($id);

        if (empty($product)) {
            return redirect()->route('products.index');
        }

        $product->delete();

        return redirect()->route('products.index');
    }

    public function view($id){
        $product = Products::find($id);

        if (empty($product)) {
            return redirect()->route('products.index');
        }

        return view('products.detail', ['product' => $product]);
    }

    public function addImage($productId){
        $product = Products::find($productId);
        $files = Images::select('id', 'name', 'location')->get();

        return view('products.add-image', ['files' => $files, 'product' => $product]);
    }

    public function updateImage(Request $request, $productId){
        return view('products.add-image');
    }

    public function getAddtoCart(Request $request, $id)
    {
        $product = Products::find($id);
        $oldCart = Session('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);
        $request->session()->put('cart', $cart);
        return view('products.detail')->with('product', $product);
    }
}
