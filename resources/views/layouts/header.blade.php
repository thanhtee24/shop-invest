<header>
    <!-- header inner -->
    <div class="header">
        <div class="header_to d_none">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <ul class="lan">
                            <li><i class="fa fa-globe" aria-hidden="true"></i> Language : <img src="images/fleg.png" alt="#" /></li>
                        </ul>
                        <form action="#">
                            <div class="select-box">
                                <label for="select-box1" class="label select-box1"><span class="label-desc">English</span> </label>
                                <select id="select-box1" class="select">
                                    <option value="Choice 1">English</option>
                                    <option value="Choice 1">Falkla</option>
                                    <option value="Choice 2">Germa</option>
                                    <option value="Choice 3">Neverl</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <ul class="social_icon1">
                            <li> F0llow Me
                            </li>
                            <li> <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"> <i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_midil">
            <div class="container">
                <div class="row d_flex">
                    <div class="col-md-4 col-sm-4 d_none">
                        <ul class="conta_icon">
                            <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i> Call Me : +84 907 36 20 48</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <a class="logo" href="#"><img src="images/logo.png" alt="#" /></a>
                    </div>
                    <div class="col-md-4 col-sm-4 d_none">
                        <ul class="conta_icon ">
                            <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> demo@gmail.com</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.menu')
    </div>
</header>
