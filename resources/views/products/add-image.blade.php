<x-master>
    <h1>Add Image For Product</h1>
    <div class="container">
        <div class="table-bordered">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">name</th>
                    <th scope="col">Image</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                    <tr>
                        <td class="text-center-fixed">{{data_get($file, 'name')}}</td>
                        <td><img src="{{asset('storage/'.data_get($file, 'location'))}}" alt="" height="100px"/></td>
                        <td><a class="btn btn-default text-white bg-blue-active"
                               href="{{route("products.update-image", data_get($product, 'id'))}}">Add</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</x-master>
