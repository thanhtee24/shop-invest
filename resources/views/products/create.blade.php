<x-master>
    <form method="post" action="{{route('products.store')}}">
        <div class="container">
            <div class="inputBox">
                <input class="form-control" name="name" placeholder="ProductName" type="text" required="">
            </div>
            <div class="inputBox" >
                <input class="form-control" name="price" placeholder="Price" type="text" required="">
            </div>
            <select name="brand_id" class="form-control">
                <option value="">Select Brand</option>
                @foreach($brands as $id => $brand)
                    <option value="{{$id}}">{{$brand}}</option>
                @endforeach
            </select>
            <div class="inputBox">
                <textarea class="form-control" name="description" placeholder="Description" type="text" required=""></textarea>
            </div>
            <div class="inputBox">
                <input class="btn btn-default text-white bg-danger" type="submit" name="create" value="Create">{{ csrf_field() }}
                <a class="btn btn-default text-white bg-danger" href="{{route('products.index')}}">Back</a>
            </div>
        </div>
    </form>
</x-master>
