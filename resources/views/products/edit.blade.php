<x-master>
    <form method="post" action="{{route('products.update', data_get($product, 'id'))}}">
        <div class="container">
            <div class="inputBox">
                <input class="form-control" name="name" placeholder="ProductName" type="text" value="{{data_get($product, 'name')}}">
            </div>
            <div class="inputBox" >
                <input class="form-control" name="price" placeholder="Price" type="text" value="{{data_get($product, 'price')}}">
            </div>
            <select name="brand_id" class="form-control" value="{{data_get($product, 'brand_id')}}">
                <option value="">Select Brand</option>
                @foreach($brands as $id => $brand)
                    <option value="{{$id}}">{{$brand}}</option>
                @endforeach
            </select>
            <div class="inputBox">
                <input class="form-control" name="description" placeholder="Description" type="text" value="{{data_get($product, 'description')}}">
            </div>
            <div class="form-group col-sm-12">
                <input class="btn btn-default text-white bg-success" type="submit" name="update" value="Update">{{ csrf_field() }}
                <a class="btn btn-default text-white bg-danger" href="{{route('products.index')}}">Back</a>
            </div>
        </div>
    </form>
</x-master>
