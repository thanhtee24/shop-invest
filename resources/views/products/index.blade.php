<x-master>
    <h1>Products</h1>
    <a class="btn btn-default text-white bg-blue-active" href="{{route("products.create")}}">Create</a>
    <a class="btn btn-default text-white bg-blue-active" href="{{route("images.create-image")}}">Upload Image</a>
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">name</th>
                <th scope="col">Brand</th>
                <th scope="col">Price</th>
                <th scope="col">Description</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <th scope="row">{{data_get($product, 'id')}}</th>
                    <td class="text-center-fixed">{{data_get($product, 'name')}}</td>
                    <td class="text-center-fixed">{{data_get($product->brand, 'name')}}</td>
                    <td class="text-center-fixed">{{data_get($product, 'price')}}</td>
                    <td class="text-center-fixed col-3">{{data_get($product, 'description')}}</td>
                    <td class="text-center-fixed">
                        <a class="btn btn-default text-white bg-blue-active"
                           href="{{route("products.view", data_get($product, 'id'))}}">View</a>
                        <a class="btn btn-default text-white bg-blue-active"
                           href="{{route("products.add-image", data_get($product, 'id'))}}">Add Image</a>
                        <a class="btn btn-default text-white bg-blue-active"
                           href="{{route("products.edit", data_get($product, 'id'))}}">Edit</a>
                        <a class="btn btn-default text-white bg-danger"
                           href="{{route("products.destroy", data_get($product, 'id'))}}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</x-master>
