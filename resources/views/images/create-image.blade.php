<x-master>
    <form method="post" action="{{route('images.upload-image')}}" enctype="multipart/form-data">
        <div class="container">
            <div class="inputBox">
                <input class="form-control" name="file-image" placeholder="File" type="file">
            </div>
            <div class="inputBox">
                <input class="btn btn-default text-white bg-danger" type="submit" name="upload" value="Upload">{{ csrf_field() }}
                <a class="btn btn-default text-white bg-danger" href="{{route('products.index')}}">Back</a>
            </div>
        </div>
    </form>
</x-master>
